import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'

export default createStore({
	plugins: [createPersistedState()],
	state: {
		cart: {},
	},
	getters: {
		getCart(state) {
			return state.cart
		},
		isCartEmpty(state) {
			return Object.keys(state.cart).length === 0;
		},
		getCount: state => id => {
			if (!state.cart[id]) {
				return 0
			}

			return state.cart[id].count

			// for (var i = 0; i < state.cart.length; i++) {
			// 	if (state.cart[i].id === id) {
			// 		if (state.cart[i].count) {
			// 			return state.cart[i].count
			// 		}
			// 	}
			// }
			// return 0 
		},
	},
	mutations: {
		pushToCart(state, item) {
			// -- variant #1 --
			// 		if (!item) return
			// 		for (var i = 0; i < state.cart.length; i++) {
			// 			if (state.cart[i].id == item.id) {
			// 				if (state.cart[i].count) {
			// 					state.cart[i].count++
			//       }
			// 				return
			// 			}
			//   }
			//   item.count = 1
			// 		state.cart.push(item)

			// -- variant #2 --
			// if (!item) return
			
			// const productIndex = state.cart.findIndex(product => product.id === item.id)

			// if ( productIndex > -1)  {
			// 	state.cart[productIndex].count++;
			// 	return;
			// }

			// state.cart.push({...item, count: 1});

			// -- variant 3 --
			if (!state.cart[item.id]) {
				state.cart[item.id] = { ...item, count: 1 }
			} else {
				state.cart[item.id].count++
			}
		},
		deleteOfCart(state, item) {
			if (!item || !state.cart[item.id]) return

			// for (var i = 0; i < state.cart.length; i++) {
			// 	if (state.cart[i].id == item.id) {
			// 		if (state.cart[i].count) {
			// 			state.cart[i].count--

			// 			if (state.cart[i].count == 0) {
			// 				state.cart = state.cart.filter(element => element.id !== item.id)
			// 			}
			// 		}

			// 		return
			// 	}
			// }

			if (state.cart[item.id].count > 1) {
				state.cart[item.id].count--
			} else {
				delete state.cart[item.id]
			}
		}
	},
})